import sbt._

object Dependencies {

  val hammock      = "com.pepegar" %% "hammock-core"  % Versions.hammock

  val hammockCirce = "com.pepegar" %% "hammock-circe" % Versions.hammock

  val scalactic = "org.scalactic" %% "scalactic" % Versions.scalactic

  val scalaTest = "org.scalatest" %% "scalatest" % Versions.scalaTest % "test"

}