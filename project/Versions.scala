object Versions {

  val hammock = "0.8.1"

  val scalactic = "3.0.5"

  val scalaTest = "3.0.5"

}