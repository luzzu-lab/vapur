package luzzu.vapur.engine.marshalling

import org.scalatest.{ FlatSpec, Matchers }

import io.circe.parser.parse

import luzzu.vapur.engine.PortData

class PortDataMarshallingSpec extends FlatSpec with Matchers {

  private val portData = PortData(ip = "127.0.0.1", privatePort = 2375, publicPort = 2375, socketType = "tcp")
  private val portDataJson =  parse {
    s"""
       |{
       |  "IP": "${portData.ip.getOrElse("unknown")}",
       |  "PrivatePort": ${portData.privatePort},
       |  "PublicPort": ${portData.publicPort},
       |  "Type": "${portData.socketType}"
       |}
     """.stripMargin
  }

  "PortData marshalling" should "decode Json" in  {
    portDataJson.flatMap { PortDataMarshalling.portDataDecoder.decodeJson } should be { Right(portData) }
  }

}