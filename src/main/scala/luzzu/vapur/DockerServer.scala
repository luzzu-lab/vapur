package luzzu.vapur

import hammock.{ Uri => HammockUri }

sealed trait DockerServer {

  def path: String

  def protocol: String

  final def uri = HammockUri(path = s"$protocol://$path")

}

sealed class TcpServer(host: String, port: Int) extends DockerServer {

  final def path = s"$host:$port"
  final def protocol = "http"

}

sealed class UnixServer(val path: String) extends DockerServer {

  final def protocol = "unix"

}

object DockerServer {

  def default: DockerServer = new UnixServer("/var/run/docker.sock")
  def unix(path: String): DockerServer = new UnixServer(path)
  def tcp(host: String, port: Int): DockerServer = new TcpServer(host, port)

}