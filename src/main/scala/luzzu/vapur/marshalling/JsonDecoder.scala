package luzzu.vapur.marshalling

import cats.syntax.traverse.toTraverseOps
import cats.instances.list.catsStdInstancesForList
import cats.instances.either.catsStdInstancesForEither

import io.circe.{ Decoder, ACursor, HCursor }

object JsonDecoder {

  def custom[A](f: HCursor => Decoder.Result[A]): Decoder[A] = f(_)

  implicit class ACursorFunctions(cursor: ACursor) {

    def downKeys[A](f: String => Decoder[A]): Decoder.Result[Seq[A]] =
      cursor.keys.getOrElse { Iterable.empty[String] }.toList.traverse[Decoder.Result, A] { key =>
        cursor.downField(key).as[A](f(key))
      }

  }

}
