package luzzu.vapur

import hammock.{ Uri => HammockUri }

object Uri {

  implicit class BaseUri(baseUri: HammockUri) {

    def /(path: HammockUri): HammockUri = baseUri / path.path

  }

}
