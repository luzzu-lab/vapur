package luzzu.vapur.engine

import luzzu.vapur.api.Response

case class ContainerData(id: String,
                         names: Seq[String],
                         image: String,
                         imageId: String,
                         command: String,
                         created: Long,
                         state: String,
                         status: String,
                         ports: Seq[PortData],
                         labels: Map[String, String],
                         sizeRaw: Option[Int],
                         sizeRootFs: Option[Int],
                         hostConfig: Map[String, String],
                         networkSettings: Seq[Network],
                         mounts: Seq[Mount]) extends Response

case class PortData(ip: Option[String], privatePort: Int, publicPort: Int, socketType: String)

object PortData {

  def apply(ip: String, privatePort: Int, publicPort: Int, socketType: String): PortData = apply(Some(ip), privatePort, publicPort, socketType)

}

case class Network(name: String,
                   networkId: String,
                   endpointId: String,
                   gateway: String,
                   ipAddress: String,
                   ipPrefixLength: Int,
                   ipv6Gateway: String,
                   globalIPv6Address: String,
                   globalIPv6PrefixLength: Int,
                   macAddress: String)

case class Mount(name: String,
                 mountType: String,
                 source: String,
                 destination: String,
                 driver: Option[String],
                 mode: String,
                 canWrite: Boolean,
                 propagation: String)