package luzzu.vapur.engine

import io.circe.Decoder.decodeSeq

import hammock.{ Method, Uri }

import luzzu.vapur.api._
import luzzu.vapur.api.QueryParameters.Name
import luzzu.vapur.engine.marshalling.ContainerDataMarshalling

sealed abstract class EngineRequest(protected val path: String) extends Request {

  def base = "containers"

  def uri = Uri(path = s"$base/$path", query = queryParams.toMap)

}

private[engine] case class EngineListRequest(all: Boolean = false, limit: Option[Int] = None, includeSizes: Boolean = false) extends EngineRequest("json") {

  protected type Reply = Seq[ContainerData]

  override protected implicit def replyDecoder = decodeSeq(ContainerDataMarshalling.containerDataDecoder)

  def method = Method.GET

  def showAll = this.copy(all = true)

  def showRunning = this.copy(all = false)

  def limit(to: Int) = this.copy(limit = Some(to))

  def showSizes = this.copy(includeSizes = true)

  def hideSizes = this.copy(includeSizes = false)

  override def queryParams = { "all" := all } &
    { "limit" ?= limit        } &
    { "size"  := includeSizes }

}

private[engine] object EngineListRequest {

  def default = apply()

}