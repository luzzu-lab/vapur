package luzzu.vapur.engine.marshalling

import scala.language.implicitConversions

import io.circe.Decoder

import luzzu.vapur.marshalling.JsonDecoder
import luzzu.vapur.engine.Mount

object MountMarshalling {

  implicit def mountDecoder: Decoder[Mount] = JsonDecoder.custom { cursor =>
    for {
      name        <- cursor.downField("Name").as[Option[String]]
      mountType   <- cursor.downField("Name").as[Option[String]]
      source      <- cursor.downField("Source").as[String]
      destination <- cursor.downField("Destination").as[String]
      driver      <- cursor.downField("Driver").as[Option[String]]
      mode        <- cursor.downField("Mode").as[String]
      canWrite    <- cursor.downField("RW").as[Boolean]
      propagation <- cursor.downField("Propagation").as[String]
    } yield Mount(
      name        = name.getOrElse { "" },
      mountType   = mountType.getOrElse { "unknown" },
      source      = source,
      destination = destination,
      driver      = driver,
      mode        = mode,
      canWrite    = canWrite,
      propagation = propagation
    )
  }

}
