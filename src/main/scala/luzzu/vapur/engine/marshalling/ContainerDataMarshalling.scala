package luzzu.vapur.engine.marshalling

import scala.language.implicitConversions

import io.circe.Decoder

import luzzu.vapur.marshalling.JsonDecoder
import luzzu.vapur.engine.{ ContainerData, PortData, Network, Mount }

import MountMarshalling.mountDecoder
import NetworkMarshalling.networksDecoder
import PortDataMarshalling.portDataDecoder

object ContainerDataMarshalling {

  implicit def containerDataDecoder: Decoder[ContainerData] = JsonDecoder.custom { cursor =>
    for {
      id               <- cursor.downField("Id").as[String]
      names            <- cursor.downField("Names").as[Seq[String]]
      image            <- cursor.downField("Image").as[String]
      imageId          <- cursor.downField("ImageID").as[String]
      command          <- cursor.downField("Command").as[String]
      created          <- cursor.downField("Created").as[Long]
      state            <- cursor.downField("State").as[String]
      status           <- cursor.downField("Status").as[String]
      ports            <- cursor.downField("Ports").as[Seq[PortData]]
      labels           <- cursor.downField("Labels").as[Map[String, String]]
      sizeRaw          <- cursor.downField("SizeRw").as[Option[Int]]
      sizeRootFs       <- cursor.downField("SizeRootFs").as[Option[Int]]
      hostConfig       <- cursor.downField("HostConfig").as[Map[String, String]]
      networkSettings  <- cursor.downField("NetworkSettings").downField("Networks").as[Seq[Network]]
      mounts           <- cursor.downField("Mounts").as[Seq[Mount]]
    } yield ContainerData(
      id              = id,
      names           = names,
      image           = image,
      imageId         = imageId,
      command         = command,
      created         = created,
      state           = state,
      status          = status,
      ports           = ports,
      labels          = labels,
      sizeRaw         = sizeRaw,
      sizeRootFs      = sizeRootFs,
      hostConfig      = hostConfig,
      networkSettings = networkSettings,
      mounts          = mounts
    )
  }

}
