package luzzu.vapur.engine.marshalling

import scala.language.implicitConversions

import io.circe.Decoder

import luzzu.vapur.marshalling.JsonDecoder
import luzzu.vapur.engine.PortData

object PortDataMarshalling {

  implicit def portDataDecoder: Decoder[PortData] = JsonDecoder.custom { cursor =>
    for {
      ip          <- cursor.downField("IP").as[Option[String]]
      privatePort <- cursor.downField("PrivatePort").as[Int]
      publicPort  <- cursor.downField("PublicPort").as[Int]
      socketType  <- cursor.downField("Type").as[String]
    } yield PortData(
      ip          = ip,
      privatePort = privatePort,
      publicPort  = publicPort,
      socketType  = socketType
    )
  }

}
