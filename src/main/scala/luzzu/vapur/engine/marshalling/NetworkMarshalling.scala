package luzzu.vapur.engine.marshalling

import scala.language.implicitConversions

import io.circe.Decoder

import luzzu.vapur.marshalling.JsonDecoder
import luzzu.vapur.marshalling.JsonDecoder.ACursorFunctions
import luzzu.vapur.engine.Network

object NetworkMarshalling {

  implicit def networksDecoder: Decoder[Seq[Network]] = JsonDecoder.custom { _.downKeys[Network](NetworkMarshalling.networkDecoder) }

  implicit def networkDecoder(name: String): Decoder[Network] = JsonDecoder.custom { cursor =>
    for {
      networkId              <- cursor.downField("NetworkID").as[String]
      endpointId             <- cursor.downField("EndpointID").as[String]
      gateway                <- cursor.downField("Gateway").as[String]
      ipAddress              <- cursor.downField("IPAddress").as[String]
      ipPrefixLength         <- cursor.downField("IPPrefixLen").as[Int]
      ipv6Gateway            <- cursor.downField("IPv6Gateway").as[String]
      globalIPv6Address      <- cursor.downField("GlobalIPv6Address").as[String]
      globalIPv6PrefixLength <- cursor.downField("GlobalIPv6PrefixLen").as[Int]
      macAddress             <- cursor.downField("MacAddress").as[String]
    } yield Network(
      name                   = name,
      networkId              = networkId,
      endpointId             = endpointId,
      gateway                = gateway,
      ipAddress              = ipAddress,
      ipPrefixLength         = ipPrefixLength,
      ipv6Gateway            = ipv6Gateway,
      globalIPv6Address      = globalIPv6Address,
      globalIPv6PrefixLength = globalIPv6PrefixLength,
      macAddress             = macAddress
    )
  }

}
