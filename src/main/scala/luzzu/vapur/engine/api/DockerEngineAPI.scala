package luzzu.vapur.engine.api

import luzzu.vapur.api.DockerAPI
import luzzu.vapur.engine.EngineListRequest

trait DockerEngineAPI { this: DockerAPI =>

  def list: EngineListRequest = EngineListRequest.default

}
