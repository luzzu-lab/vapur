package luzzu.vapur.api

import cats.effect.IO

import io.circe.Decoder

import hammock.{ Method, Hammock, Uri }
import hammock.circe.implicits.circeDecoderAuto
import hammock.jvm.Interpreter

import luzzu.vapur.DockerServer
import luzzu.vapur.Uri.BaseUri

trait Request {

  protected type Reply

  protected implicit def replyDecoder: Decoder[Reply]

  private implicit val interpreter: Interpreter[IO] = Interpreter[IO]

  def method: Method

  def uri: Uri

  def queryParams: QueryParameters = QueryParameters.empty

  def send(implicit server: DockerServer) = Hammock.request(method, server.uri / uri, Map.empty).as[Reply].exec[IO]

}

sealed case class QueryParameters(params: Seq[QueryParameter[_]]) {

  def &(param: QueryParameter[_]) = if (param.isDefined) this.copy(this.params :+ param) else this

  def toMap= params.collect {
    case SimpleQueryParameter(name, value)         => name -> value.toString
    case OptionalQueryParameter(name, Some(value)) => name -> value.toString
  }.toMap[String, String]

  override def toString = if (params.isEmpty) "" else s"?${params.mkString("&")}"

}

object QueryParameters {

  implicit class Name(name: String) {

    def :=[A](value: A): QueryParameter[A] = SimpleQueryParameter(name, value)

    def ?=[A](value: Option[A]): QueryParameter[Option[A]] = OptionalQueryParameter(name, value)

    def ??=[A](value: A): QueryParameter[Option[A]] = OptionalQueryParameter(name, Option(value))

  }

  def empty = QueryParameters(Seq.empty)

  def init(params: QueryParameter[_]*): QueryParameters = apply(params)

}

sealed abstract class QueryParameter[+A](name: String, value: A) {

  def isOptional = false

  def isDefined = true

  def unDefined = !isDefined

  def &[B](other: QueryParameter[B])= QueryParameters.init(this, other)

  override def toString = s"$name=$value"

}

sealed case class SimpleQueryParameter[+A](name: String, value: A) extends QueryParameter[A](name, value)

sealed case class OptionalQueryParameter[+A](name: String, value: Option[A]) extends QueryParameter[Option[A]](name, value) {

  override def isOptional = true

  override def isDefined= value.isDefined

  override def toString = value match {
    case Some(aa) => s"$name=$aa"
    case None     => ""
  }
}