import sbt._

organization := "luzzu"

name         := "vapur"

scalaVersion := Library.scalaVersion

libraryDependencies += Library.dependencies.hammock

libraryDependencies += Library.dependencies.hammockCirce

libraryDependencies += Library.dependencies.scalactic

libraryDependencies += Library.dependencies.scalaTest

testOptions in Test += Tests.Filter { _ endsWith "Spec" }

lazy val root = Project(id = "vapur", base = Library.base)


